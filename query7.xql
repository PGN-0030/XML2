<result>
 { for $n in doc("pessoas.xml")//pessoas/pessoa/compras/valor/text() 
   where ($n > 200) 
   order by $n ascending
   return <valor>{$n}</valor> }
</result>