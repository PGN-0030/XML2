(:  Isto é um comentário  :)

(:
F -> For faz uma iteracao no objeto
L -> Let cria ou atribui variaveis
O -> Order 
W -> Where (comparação) 
R -> Return (Return)
:)

let $pessoa := doc("pessoas.xml")//pessoas/pessoa
for $nome in $pessoa/nome
where ($nome='Janaina')
order by $nome 
return <pessoa><nome>{$nome/text()}</nome></pessoa>
