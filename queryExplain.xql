(: FLWOR 
 
 F: FOR para iterar sobre os resultados 
 L: LET serve para setar uma variável 
 W: WHERE serve para avaliar condições 
 O: ORDER serve para ordenar os resultados 
 R: RETURN serve para retornar os resultados 
 Esse acrônimo é conhecido como FLOWER ... Falta só a letra E :)

let $pessoa := doc("pessoas.xml")/pessoas/pessoa
for $nome in $pessoa/nome
return <nome>{$nome/text()}</nome>

(:return <nome>{$pessoa/nome}</nome> :)