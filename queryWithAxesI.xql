(: Query usando axes do xpath :)

for $compras in doc("pessoas.xml")/pessoas/pessoa/compras
where ($compras/valor > 300)
for $codigo in $compras/parent::pessoa/codigo 
return $codigo/text()